<?PHP
/**
 * @file
 * Handles the library functions for code_tracker_ family of modules
 */

/**
 * Creates the time in microseconds
 *
 *
 * @param $head_elements
 *   Default function parameter
 * @return 
 *   The time in microseconds
 */

function micro_maker(){

  $micro = microtime();
  $data = explode(" ", $micro);
  return (float)$data[1] . substr($data[0],1);

}

/**
 * Verifies the syntax of the given e-mail address.
 *
 * @param $location
 *   String representing the drupal hook invoked
 *
 * @return
 *   Data - object containing memory and time attributes for this hook call
 */

function make_data($location){

  $data = new StdClass;
  $data->call = $location;
  $data->memory_php = memory_get_usage();
  $data->memory_all = memory_get_usage(true);
  $data->microtime = micro_maker();
  return $data;

}

/**
 * Displays the content of the record hook tracings
 *
 * @param $thread_request
 *   Number representing this thread
 *
 */

function code_tracker_data($thread_request) {

  global $user;

  if (user_access("administer")) {

      $mem_total = ini_get("memory_limit");

      $memory = substr($mem_total, 0, strlen($mem_total)-1) . "000000";

      $last_mem_all = 0;
      $last_mem_php = 0;
      $last_time = 0;

      if (isset($_SESSION['thread_' . $thread_request])){

        $data = $_SESSION['thread_' . $thread_request];

        $block['content'] = '<div id="tracker">
                            <p>Total time for ' . request_uri() . ' to load = ' . ($data[count($data)-1]->microtime - $data[0]->microtime) . '</p>
                            <div class="hook">Hook called</div>	
                            <div class="time">Time</div>
                            <div class="time">Time (changed)</div>
                            <div class="memory">Memory</div>
                            <div class="memory">Change</div>
                            <div class="memory">Percent usage</div>
                            </div>';

        foreach($data as $info){

          $block['content'] .='<div class="tracker_row">
          <div class="hook forty">' . $info->call .'</div>
          <div class="time forty">' . $info->microtime . '</div>'; 

          if ($last_time!==0) {

            $block['content'] .= '<div class="time forty">' . ((float)$info->microtime-(float)$last_time);

          }
          else{

            $block['content'] .= '<div class="time forty">0';

          }

          $last_time = $info->microtime;

          $block['content'] .= '</div><div class="memory_row forty">';

          $block['content'] .= '<div>' . $info->memory_all . "</div>"; 

          if ($last_mem_all!==0) {

            if ((integer) $last_mem_all > (integer)$info->memory_all) {

              $block['content'] .= '<div>-' . ((integer)$last_mem_all - (integer)$info->memory_all) . "</div>";

            }
            else{

              $block['content'] .= '<div>' . ((integer)$info->memory_all - (integer)$last_mem_all) . "</div>";

            }

          }
          else{

            $block['content'] .= '<div>0</div>';

          }

          $block['content'] .= '<div>' . (((integer)$info->memory_all/(integer)$memory)*100) . '</div>';

          $last_mem_all = $info->memory_all;

          $block['content'] .= '<div>' . $info->memory_php . "</div>"; 

          if ($last_mem_php !== 0) {

            if ((integer) $last_mem_php > (integer) $info->memory_php) {

              $block['content'] .= '<div>-' . ((integer)$last_mem_php - (integer)$info->memory_php) . "</div>"; 

            }
            else{

              $block['content'] .= '<div>' . ((integer)$info->memory_php - (integer)$last_mem_php) . "</div>"; 

            }

          }
          else{

          $block['content'] .= '<div>0</div>';

        }

        $block['content'] .= '<div>' . (((integer)$info->memory_php / (integer)$memory)*100) . "</div>";

        $last_mem_php = $info->memory_php;

        $block['content'] .= '</div>';

      }


    }

    return $block;

  }

}